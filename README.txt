Tadaa! Supervision
==================

http://drupal.org/project/tadaa_supervision

What Is This?
-------------

This is a module to add supervision into the Tadaa! module.

Description
-----------

The purpose is to receive a weekly email alert on a supervision address or add an automatic supervision. 
You choose your default environment, the better environment for you.
You can enable supervision or not for each environment and manage it.
You choose periodicity and enter an address email (no address email if you use an automatic supervision, just the periodicity). 
By default, this module works with cron and the shipment will be made after 2 p.m. 
The received email is just an information to call back your oversight and it contain a link to change environment.
Or if you choose an automatic supervision, your default environment is loaded when the cron is launched and you don't receive email.



How To Install The Modules
--------------------------

1. Install Tadaa! Supervision and after installation you will be redirected to 'admin/config/development/tadaa/configure/supervision'.

2. Choose your default environment into table and edit these configurations.

3. Check the checkbox who precises the default environment.

4. Edit any environments than you've created and add manually or automatic supervision.

If you find a problem, incorrect comment, obsolete or improper code or such,
please search for an issue about it at http://drupal.org/project/issues/tadaa_supervision
If there isn't already an issue for it, please create a new one.