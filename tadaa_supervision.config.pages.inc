<?php

/**
 * Page callback for the environment overview.
 *
 * This will display a table with the environments, and links to configure supervision for each environments.
 */
function tadaa_supervision_configuration_page() {
// Load the environments.
  $environments = variable_get('tadaa_environments', array());

  if (count($environments) < 1) {
    // No environments has been added yet.
    return array(
      '#markup' => t('No environments has been added.'),
    );
  }

  // The header for the table.
  $header = array(
    t('Environment'),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  // Build one row per environment.
  $rows = array();
  $environments = variable_get('tadaa_environments', array());
  foreach ($environments as $name => $title) {
    // Build the row.
    $rows[] = array(
      $title,
      l(t('edit'), 'admin/config/development/tadaa/configure/supervision/' . $name . '/edit'),
    );
  }

  // Return a renderable table array.
  $table = array(
    '#type' => 'table',
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $table;
}

/**
 * Configuration form for supervision.
 */
function tadaa_supervision_form($form, &$form_state, $environment_name = NULL) {
  module_load_include('inc', 'tadaa', 'tadaa.callbacks');  
  
  if (isset($environment_name)) {
    // We're editing an environment, load the title.
    $environments = variable_get('tadaa_environments', array());
    if (!isset($environments[$environment_name])) {
      // Failed to load the specified environment. Display an error message to
      // the user, and return a page not found.
      // TODO: Is this the proper way to return a page not found?
      drupal_set_message(t('%name is not a valid environment.', array('%name' => $environment_name)), 'error');
      drupal_not_found();
      die();
    }
    $environment_title = $environments[$environment_name];
  }

  // Set the page title.
  if (isset($environment_title)) {
    drupal_set_title(t('Edit @title', array('@title' => $environment_title)));
  }
  
  $environment_supervision = variable_get('tadaa_'.$environment_name.'_supervision', array());
  $default_environment_exist =  variable_get('tadaa_supervision_default_environment', array());
  
  if(empty($environment_supervision)){
    $environment_name_is_default = FALSE;
    $environment_name_use_supervision = FALSE;
    $environment_name_supervision_delay = '';
    $environment_name_supervision_mail = '';
    $environment_name_supervision_auto = '';
  }else{
    $environment_name_is_default = ($environment_supervision['default_environment'] == TRUE) ? $environment_supervision['default_environment'] : FALSE;
    $environment_name_use_supervision = $environment_supervision['use_supervision'];
    $environment_name_supervision_delay = $environment_supervision['supervision_delay'];
    $environment_name_supervision_mail = $environment_supervision['supervision_mail'];
    $environment_name_supervision_auto = ($environment_supervision['supervision_automatic'] == TRUE) ? $environment_supervision['supervision_automatic'] : FALSE;
  }
  if(empty($default_environment_exist) || $default_environment_exist == $environment_name){
    $form['default_environment'] = array(
      '#type' => 'checkbox',
      '#title' => t($environment_name. ' is the default environment'),
      '#default_value'=> $environment_name_is_default,
    );
  }
  $form['use_supervision'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage supervision'),
    '#collapsed' => FALSE,
    '#header' => t('Configure when you can receive Tadaa! Supervision'),
  );
  $form['use_supervision']['environment'] = array(
    '#type' => 'hidden',
    '#value' => $environment_name,
  );
  $form['use_supervision']['supervision_for'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use supervision for '.$environment_name),
    '#default_value'=> $environment_name_use_supervision,
  );
  $form['use_supervision']['supervision_automatic'] = array(
    '#type' => 'checkbox',
    '#title' => t('It is an automatic supervision ?'),
    '#default_value'=> $environment_name_supervision_auto,
  );
  $actions = array(
    1 => t('Every mondays'),
    2 => t('Every tuesdays'),
    3 => t('Every wednesdays'),
    4 => t('Every thurdays'),
    5 => t('Every fridays'),
    6 => t('Every saturdays'),
    7 => t('Every sundays'),
  );
  $form['use_supervision']['supervision_delay'] = array(
    '#type' => 'radios',
    '#title' => t('Every how often ?'),
    '#options' => $actions,
    '#default_value' => $environment_name_supervision_delay,
    '#description' => t('Configure how often can you check your environment to receive an alert (the shipment will be made after 2 p.m)'),
  );
  $form['use_supervision']['supervision_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Send alert to :'),
    '#default_value' => $environment_name_supervision_mail,
    '#description' => t('An email address to send alert'),
    '#size' => 60,
    '#states' => array(
      'visible' => array(
        ':input[name="supervision_automatic"]' => array(
          'checked' => FALSE, 
        ),
      ),
    ),
  );
  // Action buttons.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Form submission for the supervision form.
 *
 * Save configuration for environments supervisions
 * 
 */
function tadaa_supervision_form_submit($form, &$form_state) { 
  $environments = variable_get('tadaa_environments', array());
  
  // Get the submitted values.
  if(isset($form_state['values']['default_environment']) && !empty($form_state['values']['default_environment'])){
    $default_environment = $form_state['values']['default_environment'];
  }
  $supervision_environment = $form_state['values']['environment'];
  $use_supervision = $form_state['values']['supervision_for'];
  $delay_supervision = $form_state['values']['supervision_delay'];
  $mail_supervision = $form_state['values']['supervision_mail'];
  $automatic_supervision = $form_state['values']['supervision_automatic'];
  
  // Save the supervision environments.
  variable_set('tadaa_'.$supervision_environment.'_supervision', array('use_supervision' => $use_supervision,'supervision_delay' => $delay_supervision,'supervision_mail' => $mail_supervision,'supervision_automatic' => $automatic_supervision,'default_environment' => $default_environment));
  // Save the date update
  variable_set('tadaa_mail_last', REQUEST_TIME);
  // If "Default environment" is checked, it save the default environment
  if(isset($default_environment) && $default_environment == TRUE){
    variable_set('tadaa_supervision_default_environment', $supervision_environment);
  }
  // Send a message to the user, and redirect to the environments overview.
  drupal_set_message(t('%title has been updated.', array('%title' =>  $supervision_environment)));
  $form_state['redirect'] = 'admin/config/development/tadaa/configure/supervision';
  
}